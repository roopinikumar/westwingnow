*** Settings ***
Documentation    WestwingNow Product Functionality
Resource    ../Resources/CommonFunctionality.robot
Resource    ../Resources/PageObjects/WestwingNowCookiesPopup.robot
Resource    ../Resources/PageObjects/WestwingNowHomePage.robot
Resource    ../Resources/PageObjects/PopUp.robot
Resource    ../Resources/PageObjects/MoebelDetails.robot
Resource    ../Resources/PageObjects/WishlistPage.robot

Test Setup    CommonFunctionality.Start Test Case
Test Teardown    CommonFunctionality.Finish test Case

*** Variables ***


*** Test Cases ***
Adding Product to Wishlist and deleting it
    [Documentation]    This test cases logins into WestwingNow website, adds product and deletes from Wishlist
    [Tags]    Functional
    WestwingNowCookiesPopup.Cookies Pop Up
    WestwingNowHomePage.Click on Moebel
    PopUp.Registration Pop Up
    MoebelDetails.Adding Product to Wishlist
    PopUp.Registration or Login Pop Up
    MoebelDetails.To Validate Product is added to Wishlist
    WishlistPage.Deleting Product from Wishlist Page

