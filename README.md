# WestwingNow

Project to perfrom automated UI Web testing on WestwingNow, using Robot framework. 

## Test case: Adding Product to Wishlist and deleting it

WestwingNow offers various Home and Living products. This test case is created to add and delete first found Moebel product from Wishlist.

- Products can be viewed by opening WestwingNow webpage and clicking on Moebel feature available on menu list.
- Login to website with registered account details.
- Once the product is added to Wishlist, product can be viewed in Wishlist page.
- Delete the product which is added in Wishlist page.

## Tools used for Development
- Eclipse IDE Photon Release 4.8
- RED Robot plugin 0.9.5
- Python 3.8
- Chrome 90.0.4430.212
- JRE 8
- Robotframe Selenium Library 5.1.3

## Env Setup

### Internalization
Since German special charater was used in the code, we might encounter encoding/parsing errors.
To prevent this encode the file in UTF-8.

## Running test case on CLI ( Command line )
- Python 3.5+ is required
- Robot framework python library is required
- Robot framework selenium python library is required

Run following in command prompt/Powershell to install robot framework libraries
```
pip install --upgrade robotframework
pip install --upgrade robotframework-seleniumlibrary
```
Test case is located at _"westwingnow/Task2/Product.robot"_

To run test case from commandline / powershell 
```
git clone https://gitlab.com/roopinikumar/westwingnow.git
cd westwingnow/Task2
python robot Product.robot 
```

 