*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${Moebel}    //span[text()='Möbel']
${WishlistIcon}    (//div[@class='WishlistIcon__StyledWishlistIconWrapper-sc-75dklq-0 jujCBZ'])[1]
${username}    roopini.kumar28@gmail.com
${password}    Westwinguma04!!
${Wunschliste}    //span[text()='Wunschliste']

*** Keywords ***
Cookies Pop Up
    Wait Until Element Is Visible    //button[@id="onetrust-accept-btn-handler"]
    Click Element    //button[@id="onetrust-accept-btn-handler"]
    
Click on Moebel
    Wait Until Element Is Visible    ${Moebel}
    Click Element    ${Moebel}

Registration Pop Up
    Sleep    6s
    Reload Page
    Sleep    3s

Adding Product to Wishlist
    Wait Until Element Is Visible    ${WishlistIcon}
    Click Element    ${WishlistIcon}
    
Registration or Login Pop Up
    Wait Until Element Is Visible    //button[@type='button']
    Click Button    //button[@type='button']
    Sleep    2s
    Input Text    name:email    ${username}
    Sleep    2s
    Input Text    name:password    ${password}
    Wait Until Element Is Visible    //button[@data-testid='login_reg_submit_btn']
    Click Button    //button[@data-testid='login_reg_submit_btn']
    Sleep    5s

To Validate Product is added to Wishlist
    Wait Until Element Is Visible    //span[@class='CountBubble__StyledBadgeCount-sc-15yuxo7-0 fYYOMq qa-header-wishlist-counter'] 
    Wait Until Element Is Visible    ${Wunschliste}
    Click Element    ${Wunschliste}

Deleting Product from Wishlist Page
    Wait until Element is Visible    //button[@class='blockListProduct__delete qaBlockListProduct__delete']
    Click Element    //button[@class='blockListProduct__delete qaBlockListProduct__delete']
    Sleep    5s
    Wait Until Element Is Visible    //p[@class='wishlistNoProducts__text wishlistNoProducts__text_bold']

