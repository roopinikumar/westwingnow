*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}    https://www.westwingnow.de/
${browser}    chrome

*** Keywords ***
Start Test Case
    Open Browser        ${URL}    ${browser}
    Maximize Browser Window
    
Finish test Case
    Close Browser
    
