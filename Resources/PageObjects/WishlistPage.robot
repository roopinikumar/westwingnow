*** Settings ***
Library    SeleniumLibrary

*** Variables ***

*** Keywords ***
Deleting Product from Wishlist Page
    Wait until Element is Visible    //button[@class='blockListProduct__delete qaBlockListProduct__delete']
    Click Element    //button[@class='blockListProduct__delete qaBlockListProduct__delete']
    Sleep    5s
    Wait Until Element Is Visible    //p[@class='wishlistNoProducts__text wishlistNoProducts__text_bold']