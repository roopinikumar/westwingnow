*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${Moebel}    //span[text()='Möbel']

*** Keywords ***
Click on Moebel
    Wait Until Element Is Visible    ${Moebel}
    Click Element    ${Moebel}