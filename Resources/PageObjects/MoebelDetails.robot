*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${WishlistIcon}    (//div[@class='WishlistIcon__StyledWishlistIconWrapper-sc-75dklq-0 jujCBZ'])[1]
${Wunschliste}    //span[text()='Wunschliste']

*** Keywords ***
Adding Product to Wishlist
    Wait Until Element Is Visible    ${WishlistIcon}
    Click Element    ${WishlistIcon}
    
To Validate Product is added to Wishlist
    Wait Until Element Is Visible    //span[@class='CountBubble__StyledBadgeCount-sc-15yuxo7-0 fYYOMq qa-header-wishlist-counter'] 
    Wait Until Element Is Visible    ${Wunschliste}
    Click Element    ${Wunschliste}
